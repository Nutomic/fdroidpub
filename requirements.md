F-Droid and ActivityPub Integration Requirements
================================================

Definitions and Prerequesites
-----------------------------

We will host either a Masotodon or Pleroma instance, which will publish app
updates and other related information. For this document, we assume that the
instance will be hosted at `https://pleroma.fdroid.org`. We have not decided
between Mastodon or Pleroma yet, this document only refers to Pleroma for
simplicity.

Update Bot
----------

The bot frequently updates it's copy of the F-Droid index. Whenever a new index
is received, it goes through all apps, and performs one of the following
actions (or does nothing).

The bot should be written in Python. This gives us access to existing code
from the F-Droid [Repomaker](https://gitlab.com/fdroid/repomaker) and the
Pleroma API via [Mastodon.py](https://github.com/halcy/Mastodon.py).

All posts by the bot should be unlisted.

For security reasons, API access should be limited to `localhost`.
  
### App creation

When an app is encountered for the first time by the update bot, it will
create a new user account. For this, it needs the access token of an 
administrator, with permission to create new accounts.

Apps should be created by taking their English name, excluding spaces or special
characters. Alternatively, we could add a new field `PleromaUser` to the F-Droid
metadata.

Registering an account should automatically populate the profile, including 
description, profile picture (app icon) and project URL. The bot also needs to
create an access token, and store it in the database.

```
table app_info
+----+-------------------+--------------+--------------+
| id | package_name      | account_name | access_token |
+----+-------------------+--------------+--------------+
| 1  | org.fdroid.fdroid | fdroid       | dwiaofesafn  |
+----+-------------------+--------------+--------------+
```

If the account for an app was created for the first time, the bot should:
- not post anything?
- post only the latest changelog?
- post all changelogs?
- post some kind of "hello world" message?

Note regarding name changes: changing the name of an app should happen very rarely,
so this does not need to be handled by the bot. Instead, an administrator can
manually configure the old account as 
"[moved to](https://github.com/tootsuite/mastodon/pull/5832)" the new account.
Hopefully Pleroma has a similar feature.

### App updates

When an app is updated, the bot creates a new toot containing the app changelog.

How to handle post length limit?
- we could increase the limit (this might be annoying for Mastodon users)
- cut off post after limit, post link for full changelog
- use content warning to fold long post

The bot needs to keep track of all updates that have been posted already, by
storing them in an SQLite database.

```
table changelogs_posted
+--------+--------------+
| app_Id | version_name |
+--------+--------------+
| 1      | 1.0.1        |
+--------+--------------+
```

Changes to Pleroma
------------------

There should be a new public RSS/Atom feed for each account, which contains
all mentions of that user. These are the criteria to include a toot:
- toot is public
- begins with `@appname@pleroma.fdroid.org` (eg the format used when clicking
   "Mention" on a user's profile)
- does not contain any other mentions
- is the start of a new thread (eg not a reply to another toot)

This feed will be displayed inside the F-Droid client, as comments on the app.

Changes to F-Droid Client
-------------------------

- display mentions as comments (via RSS/Atom)
- allow interacting via ActivityPub, buttons to add comment, follow etc
  (using Tusky, Mastolab or Browser)

Other Considerations
--------------------
- developers may use the app's account to post on their own (but update 
  news would still be posted by the bot)
- there should be an option to disable the bot for a specific app if
  the developer is not interested
- Registration will be disabled on the Pleroma instance. The only users
  will be our bot, moderators, and app developers (which are given access
  to their respective bot).
  
Possible Future Additions
-------------------------

- Internationalization
- Easy deployment with Docker
- possibly show changelog and developer comments in F-Droid client
- possibly use number of followers to indicate popularity in F-Droid client 
  (eg for search, ranking)
- special bot accounts that post additional information
  - last_updated reposts packages' posts

  - latest reposts packages' first posts (... got newly published)

  - status posts statistics ("3 new packages got added", "24 packages got
    updated", "The most used permission inthis repo is
    android.permission.INTERNET")